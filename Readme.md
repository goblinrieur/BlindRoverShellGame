# blind robot simulator

Idea of the game is to simulate your rover remote control 

even after the attack of an alien who broke cam & mic

you can give instruction authorized of course

but you are also allowed to use your own forth function 

during the game. 

# DISCLAIMER

This is not a real project but a toy to learn & play with gforth language itself 

## a party will look like that in terminal : 

![screenshot](./local.png)

# Can you participate to code & doc ?

With pleasure, yes.

Contact me, _(see below for contact mail)_.

## Can You fork it ? 

Yes, let me know & follow you work :) 

## Using licence ? 

Finally choose to use the Unlicensed [LICENSE](./LICENSE)

## Contact me :

You can contact me on mail  : [goblinrieur@gmail.com](mailto://goblinrieur@gmail.com)

or join me on  : [discord](https://discord.gg/9szvduB)

[youtube](https://www.youtube.com/channel/UCnsW_UH9vXX-nBe3X-enXYA)

# Special conditions to win:

Player have to both find the rock and the alien first 

then he goes to the extraction point 

# current state 

Player start with some instructions

The game is usable/playable 

- where
- wait
- pause
- batterystatus
- solarpaneldeploy
- solarcapture
- solarpaneloff
- armdeploy
- armoff
- catchobject
- captureobject
- reset
- checkmove
- north
- south
- east
- west
- northeast
- southeast
- southwest
- northwest
- ping
- help
- transmit
- extract
- autodestroy
- etc.

[code](BRS.fs)

# FUTURE : 

Wed May 31 10:48:41 PM CEST 2023

- [X] update tag & version of course

- [X] colorize

- [X] title it 


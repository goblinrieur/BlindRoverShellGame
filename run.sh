#! /usr/bin/env bash

runner=$(which gforth-fast)

if [[ ${runner} != *"gforth-fast"* ]] ; then 
	 echo "Gforth interpreter not found"
	 exit 1
fi

$runner ./BRS.fs
exit $?
